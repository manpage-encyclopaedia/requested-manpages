# Requested manpages

List of desirable manpages.

- [GLM](https://glm.g-truc.net)
- [Box2D](https://github.com/erincatto/Box2D)
- [FreeType](freetype.org)
- [GLEW](http://glew.sourceforge.net/)
- [apitrace](https://github.com/apitrace/apitrace)
